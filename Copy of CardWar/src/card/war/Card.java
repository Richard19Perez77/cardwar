package card.war;

import android.graphics.Bitmap;

public class Card {
	
	int value;
	String color;
	String shape;
	int player;
	boolean isSet;
	Bitmap pic;
	
	public Card(){
		isSet = false;
	}
	
	public Card(int value, String shape){
		this.value = value;
		this.shape = shape;
		setColor();
		isSet = false;
	}

	private void setColor() {
		if (shape.equals("heart")){
			color = "red";
		}
		else if (shape.equals("diamond")){
			color = "red";
		}
		else if (shape.equals("spade")){
			color = "black";
		}
		else if (shape.equals("club")){
			color = "black";
		}
	}
	
}
