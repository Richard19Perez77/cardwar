package card.war;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	ImageView background;
	Bitmap bg, cardbw, empty;
	DisplayMetrics metrics;
	ImageButton card1, card2, card3, card4, card5, card6, card7, card8, card9,
			card10;
	Card c1, c2, c3, c4, c5, c6, c7, c8, c9, c10;
	ImageButton slotc, slotr, slotl, slottr, slottl, slotbc, slotbl, slotbr,
			slottc;
	ImageButton temp;
	Card sc, scr, scl, stc, stl, str, sbc, sbr, sbl;
	int finalHeight, finalWidth;
	Button startGame;
	Deck d;

	Card currCard;
	boolean cardSelected;

	int playerTurn, roundTurns, p1Score, p2Score, p1gamesWon, p2gamesWon;

	TextView scoreText;
	String scoreString;

	Context c;

	CheckBox cpuOn;
	boolean cpuOpponent, playSounds;
	Button rules;

	Random rand;

	ArrayList<Card> pcCards;

	ArrayList<Card> availCards;
	ArrayList<ImageButton> availImages;
	ArrayList<Integer> slotNumbers;

	int pdng;
	int fracVariable;

	int screenHeight, screenWidth, densityDpi;

	float x, y;

	int startHeight;
	boolean reduceStartButton;

	SoundPool sp;
	HashMap<Integer, Integer> soundMap;
	int s1, s2;
	float fSpeed = 1.0f;

	MenuItem soundIcon, creditsIcon;
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.sound_action:
			if (playSounds) {
				soundIcon.setIcon(R.drawable.nosoundicon);
				playSounds = false;
				break;
			} else {
				soundIcon.setIcon(R.drawable.soundicon);
				playSounds = true;
				break;
			}
		case R.id.credits:
			AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
			alertbox.setMessage(
					"Developer: Richard A. Perez\n"
							+ "Card Artwork: Kojika Sanzaki\n"
							+ "Sounds: FreeSound.org\n").setTitle("CardWAR!");
			alertbox.setPositiveButton("OK!",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							Toast.makeText(getApplicationContext(),
									"Good Luck!", Toast.LENGTH_LONG).show();
						}
					});
			alertbox.show();
			break;
		}
		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		soundIcon = menu.findItem(R.id.sound_action);
		creditsIcon = menu.findItem(R.id.credits);
		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		playSounds = true;

		sp = new SoundPool(4, AudioManager.STREAM_MUSIC, 100);
		s1 = sp.load(this, R.raw.cardselected, 1);
		s2 = sp.load(this, R.raw.cardturned, 1);

		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		screenHeight = metrics.heightPixels;
		screenWidth = metrics.widthPixels;
		densityDpi = metrics.densityDpi;

		if (screenHeight >= 750) {
			pdng = 14;
		} else {
			pdng = 10;
		}

		rand = new Random();
		pcCards = new ArrayList<Card>();

		p1gamesWon = 0;
		p2gamesWon = 0;

		c = getBaseContext();
		try {
			d = new Deck(c);
		} catch (IOException e) {
			e.printStackTrace();
		}

		startGame = (Button) findViewById(R.id.startButton);
		scoreText = (TextView) findViewById(R.id.scoreText);
		x = scoreText.getTextSize();

		switch (densityDpi) {
		case DisplayMetrics.DENSITY_LOW:
			fracVariable = 7;
			break;
		case DisplayMetrics.DENSITY_MEDIUM:
			fracVariable = 7;
			y = 24;
			scoreText.setTextSize(y);
			break;
		case DisplayMetrics.DENSITY_HIGH:
			if ((screenWidth > 790 && screenWidth < 900)
					&& (screenHeight > 460 && screenHeight < 500)) {
				reduceStartButton = true;
				y = 10;
				fracVariable = 9;
				scoreText.setTextSize(y);
			} else if ((screenWidth > 950 && screenWidth < 980)
					&& (screenHeight > 520 && screenHeight < 560)) {
				y = 12;
				fracVariable = 8;
				scoreText.setTextSize(y);
			} else {
				y = 8;
				fracVariable = 8;
				scoreText.setTextSize(y);
			}
			break;
		case DisplayMetrics.DENSITY_XHIGH:
			fracVariable = 8;
			y = 12;
			scoreText.setTextSize(y);
			break;
		case DisplayMetrics.DENSITY_XXHIGH:
			fracVariable = 9;
			break;
		case DisplayMetrics.DENSITY_TV:
			fracVariable = 7;
			break;
		default:
			fracVariable = 6;
			y = 18;
		}

		resizeCardImageButtons();
		createBackground();
		setInitialSlots();
		updateScoreBoard();

		cpuOpponent = false;

		cpuOn = (CheckBox) findViewById(R.id.cpuOnCheckBox);
		cpuOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				cpuPressed();
			}
		});

		rules = (Button) findViewById(R.id.rulesButton);
		rules.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				rulesPressed();
			}
		});
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		if (reduceStartButton) {
			ViewGroup.LayoutParams params = startGame.getLayoutParams();
			params.height = 50;
			params.width = 110;
			startGame.setTextSize(y);
			startGame.setLayoutParams(params);

			ViewGroup.LayoutParams params2 = rules.getLayoutParams();
			params2.height = 50;
			params2.width = 90;
			rules.setTextSize(y);
			rules.setLayoutParams(params2);

			ViewGroup.LayoutParams params3 = cpuOn.getLayoutParams();
			params.height = 50;
			params.width = 100;
			cpuOn.setTextSize(y);
			cpuOn.setLayoutParams(params3);

		}
	}

	public void playSound(int sound, float fSpeed) {
		if (playSounds) {
			AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
			float streamVolumeCurrent = mgr
					.getStreamVolume(AudioManager.STREAM_MUSIC);
			float streamVolumeMax = mgr
					.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
			float volume = streamVolumeCurrent / streamVolumeMax;
			sp.play(sound, volume, volume, 1, 0, fSpeed);
		}
	}

	public void rulesPressed() {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
		alertbox.setMessage(
				"a)  Player one's border is BLUE.\n"
						+ "b)  Player two's border is RED.\n"
						+ "c)  Center card starts out neutral, but can be taken.\n"
						+ "d)  Think war. The high card turns border color.\n"
						+ "e)  Try to take multiple cards at once with higher cards.\n"
						+ "f)  Ace is the highest card.\n"
						+ "g)  Check P2 cpu on for Computer Opponent.")
				.setTitle("Rules/Instructions");
		alertbox.setPositiveButton("OK!",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
						Toast.makeText(getApplicationContext(), "Good Luck!",
								Toast.LENGTH_LONG).show();
					}
				});
		alertbox.show();
	}

	public void cpuPressed() {
		if (cpuOn.isChecked()) {
			cpuOpponent = true;
			if (playerTurn == 2) {
				cpuTurn();
				switchPlayer();
			}
		} else {
			cpuOpponent = false;
		}
	}

	public void getAvailablePcCards(ArrayList<Card> availCards,
			ArrayList<ImageButton> availImages) {
		int i = 0;
		for (Card c : pcCards) {
			if (!c.isSet) {
				availCards.add(c);
				switch (i) {
				case 0:
					availImages.add(card6);
					break;
				case 1:
					availImages.add(card7);
					break;
				case 2:
					availImages.add(card8);
					break;
				case 3:
					availImages.add(card9);
					break;
				case 4:
					availImages.add(card10);
					break;
				}
			}
			i++;
		}
	}

	public void getAvailableSlots(ArrayList<Integer> slotNumbers) {
		if (!stl.isSet) {
			slotNumbers.add(0);
		}
		if (!stc.isSet) {
			slotNumbers.add(1);
		}
		if (!str.isSet) {
			slotNumbers.add(2);
		}
		if (!scl.isSet) {
			slotNumbers.add(3);
		}
		if (!sc.isSet) {
			slotNumbers.add(4);
		}
		if (!scr.isSet) {
			slotNumbers.add(5);
		}
		if (!sbl.isSet) {
			slotNumbers.add(6);
		}
		if (!sbc.isSet) {
			slotNumbers.add(7);
		}
		if (!sbr.isSet) {
			slotNumbers.add(8);
		}
	}

	private void cpuTurn() {

		availCards.clear();
		availImages.clear();
		slotNumbers.clear();

		// try a rand card in every slot
		getAvailablePcCards(availCards, availImages);
		boolean cardFound = false;

		while (!availCards.isEmpty()) {

			getAvailableSlots(slotNumbers);

			// get random card
			int r = rand.nextInt(availCards.size());
			currCard = availCards.remove(r);
			temp = availImages.remove(r);

			// try card in every avail slot
			while (!slotNumbers.isEmpty()) {

				int rs = rand.nextInt(slotNumbers.size());
				int slotN = slotNumbers.remove(rs);

				switch (slotN) {
				case 0:
					// setting in stl
					if (stc.isSet && stc.player == 1) {
						if (currCard.value > stc.value) {
							temp.setVisibility(View.INVISIBLE);
							stl = currCard;
							stl.isSet = true;
							slottl.setImageBitmap(stl.pic);
							updateBoardTopLeft();
							slotNumbers.clear();
							availCards.clear();
							cardFound = true;
						}
					} else if (scl.isSet && scl.player == 1) {
						if (currCard.value > scl.value) {
							temp.setVisibility(View.INVISIBLE);
							stl = currCard;
							stl.isSet = true;
							slottl.setImageBitmap(stl.pic);
							updateBoardTopLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 1:
					// settig in stc
					if (stl.isSet && stl.player == 1) {
						if (currCard.value > stl.value) {
							temp.setVisibility(View.INVISIBLE);
							stc = currCard;
							stc.isSet = true;
							slottc.setImageBitmap(stc.pic);
							updateBoardTopCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (str.isSet && str.player == 1) {
						if (currCard.value > str.value) {
							temp.setVisibility(View.INVISIBLE);
							stc = currCard;
							stc.isSet = true;
							slottc.setImageBitmap(stc.pic);
							updateBoardTopCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sc.isSet && sc.player == 1) {
						if (currCard.value > sc.value) {
							temp.setVisibility(View.INVISIBLE);
							stc = currCard;
							stc.isSet = true;
							slottc.setImageBitmap(stc.pic);
							updateBoardTopCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 2:
					// setting str
					if (stc.isSet && stc.player == 1) {
						if (currCard.value > stc.value) {
							temp.setVisibility(View.INVISIBLE);
							str = currCard;
							str.isSet = true;
							slottr.setImageBitmap(str.pic);
							updateBoardTopRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (scr.isSet && scr.player == 1) {
						if (currCard.value > scr.value) {
							temp.setVisibility(View.INVISIBLE);
							str = currCard;
							str.isSet = true;
							slottr.setImageBitmap(str.pic);
							updateBoardTopRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 3:
					// setting scl
					if (stl.isSet && stl.player == 1) {
						if (currCard.value > stl.value) {
							temp.setVisibility(View.INVISIBLE);
							scl = currCard;
							scl.isSet = true;
							slotl.setImageBitmap(scl.pic);
							updateBoardCenterLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sc.isSet && sc.player == 1) {
						if (currCard.value > sc.value) {
							temp.setVisibility(View.INVISIBLE);
							scl = currCard;
							scl.isSet = true;
							slotl.setImageBitmap(scl.pic);
							updateBoardCenterLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbl.isSet && sbl.player == 1) {
						if (currCard.value > sbl.value) {
							temp.setVisibility(View.INVISIBLE);
							scl = currCard;
							scl.isSet = true;
							slotl.setImageBitmap(scl.pic);
							updateBoardCenterLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 4:
					// not really needed but... sc
					if (stc.isSet && stc.player == 1) {
						if (currCard.value > stc.value) {
							temp.setVisibility(View.INVISIBLE);
							sc = currCard;
							sc.isSet = true;
							slotc.setImageBitmap(sc.pic);
							updateBoardCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (scl.isSet && scl.player == 1) {
						if (currCard.value > scl.value) {
							temp.setVisibility(View.INVISIBLE);
							sc = currCard;
							sc.isSet = true;
							slotl.setImageBitmap(sc.pic);
							updateBoardCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (scr.isSet && scr.player == 1) {
						if (currCard.value > scr.value) {
							temp.setVisibility(View.INVISIBLE);
							sc = currCard;
							sc.isSet = true;
							slotl.setImageBitmap(sc.pic);
							updateBoardCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbc.isSet && sbc.player == 1) {
						if (currCard.value > sbc.value) {
							temp.setVisibility(View.INVISIBLE);
							sc = currCard;
							sc.isSet = true;
							slotl.setImageBitmap(sc.pic);
							updateBoardCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 5:
					// check slot scr
					if (str.isSet && str.player == 1) {
						if (currCard.value > str.value) {
							temp.setVisibility(View.INVISIBLE);
							scr = currCard;
							scr.isSet = true;
							slotr.setImageBitmap(scr.pic);
							updateBoardCenterRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sc.isSet && sc.player == 1) {
						if (currCard.value > sc.value) {
							temp.setVisibility(View.INVISIBLE);
							scr = currCard;
							scr.isSet = true;
							slotr.setImageBitmap(scr.pic);
							updateBoardCenterRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbl.isSet && sbl.player == 1) {
						if (currCard.value > sbl.value) {
							temp.setVisibility(View.INVISIBLE);
							scr = currCard;
							scr.isSet = true;
							slotr.setImageBitmap(scr.pic);
							updateBoardCenterRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 6:
					// check sbl
					if (scl.isSet && scl.player == 1) {
						if (currCard.value > scl.value) {
							temp.setVisibility(View.INVISIBLE);
							sbl = currCard;
							sbl.isSet = true;
							slotbl.setImageBitmap(sbl.pic);
							updateBoardBottomLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbc.isSet && sbc.player == 1) {
						if (currCard.value > sbc.value) {
							temp.setVisibility(View.INVISIBLE);
							sbl = currCard;
							sbl.isSet = true;
							slotbl.setImageBitmap(sbl.pic);
							updateBoardBottomLeft();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 7:
					// check sbc
					if (sc.isSet && sc.player == 1) {
						if (currCard.value > sc.value) {
							temp.setVisibility(View.INVISIBLE);
							sbc = currCard;
							sbc.isSet = true;
							slotbc.setImageBitmap(sbc.pic);
							updateBoardBottomCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sc.isSet && sc.player == 1) {
						if (currCard.value > sc.value) {
							temp.setVisibility(View.INVISIBLE);
							sbc = currCard;
							sbc.isSet = true;
							slotbc.setImageBitmap(sbc.pic);
							updateBoardBottomCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbl.isSet && sbl.player == 1) {
						if (currCard.value > sbl.value) {
							temp.setVisibility(View.INVISIBLE);
							sbc = currCard;
							sbc.isSet = true;
							slotbc.setImageBitmap(sbc.pic);
							updateBoardBottomCenter();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				case 8:
					// check slot sbr
					if (scr.isSet && scr.player == 1) {
						if (currCard.value > scr.value) {
							temp.setVisibility(View.INVISIBLE);
							sbr = currCard;
							sbr.isSet = true;
							slotbr.setImageBitmap(sbr.pic);
							updateBoardBottomRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					} else if (sbc.isSet && sbc.player == 1) {
						if (currCard.value > sbc.value) {
							temp.setVisibility(View.INVISIBLE);
							sbr = currCard;
							sbr.isSet = true;
							slotbr.setImageBitmap(sbr.pic);
							updateBoardBottomRight();
							cardFound = true;
							slotNumbers.clear();
							availCards.clear();
						}
					}
					break;
				}
			}
		}

		if (!cardFound) {
			randomPcTurn();
		}
	}

	private void randomPcTurn() {

		availCards.clear();
		availImages.clear();
		getAvailablePcCards(availCards, availImages);

		int r = rand.nextInt(availCards.size());
		currCard = availCards.remove(r);
		temp = availImages.remove(r);

		slotNumbers.clear();
		getAvailableSlots(slotNumbers);

		int s;
		s = rand.nextInt(slotNumbers.size());
		int slot = slotNumbers.get(s);

		switch (slot) {
		case 0:
			temp.setVisibility(View.INVISIBLE);
			stl = currCard;
			stl.isSet = true;
			slottl.setImageBitmap(stl.pic);
			updateBoardTopLeft();
			break;
		case 1:
			temp.setVisibility(View.INVISIBLE);
			stc = currCard;
			stc.isSet = true;
			slottc.setImageBitmap(stc.pic);
			updateBoardTopCenter();
			break;
		case 2:
			temp.setVisibility(View.INVISIBLE);
			str = currCard;
			str.isSet = true;
			slottr.setImageBitmap(str.pic);
			updateBoardTopRight();
			break;
		case 3:
			temp.setVisibility(View.INVISIBLE);
			scl = currCard;
			scl.isSet = true;
			slotl.setImageBitmap(scl.pic);
			updateBoardCenterLeft();
			break;
		case 4:
			temp.setVisibility(View.INVISIBLE);
			sc = currCard;
			sc.isSet = true;
			slotc.setImageBitmap(sc.pic);
			updateBoardCenter();
			break;
		case 5:
			temp.setVisibility(View.INVISIBLE);
			scr = currCard;
			scr.isSet = true;
			slotr.setImageBitmap(scr.pic);
			updateBoardCenterRight();
			break;
		case 6:
			temp.setVisibility(View.INVISIBLE);
			sbl = currCard;
			sbl.isSet = true;
			slotbl.setImageBitmap(sbl.pic);
			updateBoardBottomLeft();
			break;
		case 7:
			temp.setVisibility(View.INVISIBLE);
			sbc = currCard;
			sbc.isSet = true;
			slotbc.setImageBitmap(sbc.pic);
			updateBoardBottomCenter();
			break;
		case 8:
			temp.setVisibility(View.INVISIBLE);
			sbr = currCard;
			sbr.isSet = true;
			slotbr.setImageBitmap(sbr.pic);
			updateBoardBottomRight();
			break;
		}
	}

	public void startGamePressed(View view) {

		availCards = new ArrayList<Card>();
		availImages = new ArrayList<ImageButton>();
		slotNumbers = new ArrayList<Integer>();

		d.shuffle();

		currCard = new Card();
		playerTurn = 1;
		roundTurns = 1;
		p1Score = 5;
		p2Score = 5;

		updateScoreBoard();

		card1.setVisibility(View.VISIBLE);
		card2.setVisibility(View.VISIBLE);
		card3.setVisibility(View.VISIBLE);
		card4.setVisibility(View.VISIBLE);
		card5.setVisibility(View.VISIBLE);
		card6.setVisibility(View.VISIBLE);
		card7.setVisibility(View.VISIBLE);
		card8.setVisibility(View.VISIBLE);
		card9.setVisibility(View.VISIBLE);
		card10.setVisibility(View.VISIBLE);

		scl = new Card();
		scr = new Card();
		stc = new Card();
		stl = new Card();
		str = new Card();
		sbr = new Card();
		sbc = new Card();
		sbl = new Card();
		sc = new Card();
		sc = d.getNextCard();
		sc.pic = getResizedBitmap(sc.pic, finalHeight, finalWidth);
		sc.isSet = true;
		sc.player = 0;

		slotc.setImageBitmap(sc.pic);
		slotc.setBackgroundColor(Color.TRANSPARENT);
		slotl.setImageBitmap(cardbw);
		slotl.setBackgroundColor(Color.TRANSPARENT);
		slotr.setImageBitmap(cardbw);
		slotr.setBackgroundColor(Color.TRANSPARENT);
		slotbc.setImageBitmap(cardbw);
		slotbc.setBackgroundColor(Color.TRANSPARENT);
		slotbl.setImageBitmap(cardbw);
		slotbl.setBackgroundColor(Color.TRANSPARENT);
		slotbr.setImageBitmap(cardbw);
		slotbr.setBackgroundColor(Color.TRANSPARENT);
		slottc.setImageBitmap(cardbw);
		slottc.setBackgroundColor(Color.TRANSPARENT);
		slottl.setImageBitmap(cardbw);
		slottl.setBackgroundColor(Color.TRANSPARENT);
		slottr.setImageBitmap(cardbw);
		slottr.setBackgroundColor(Color.TRANSPARENT);

		cardSelected = false;

		c1 = d.getNextCard();
		c2 = d.getNextCard();
		c3 = d.getNextCard();
		c4 = d.getNextCard();
		c5 = d.getNextCard();
		c6 = d.getNextCard();
		c7 = d.getNextCard();
		c8 = d.getNextCard();
		c9 = d.getNextCard();
		c10 = d.getNextCard();

		// show players cards
		c1.pic = getResizedBitmap(c1.pic, finalHeight, finalWidth);
		card1.setImageBitmap(c1.pic);
		c1.player = 1;
		card1.setBackgroundResource(R.drawable.blub);
		c2.pic = getResizedBitmap(c2.pic, finalHeight, finalWidth);
		c2.player = 1;
		card2.setImageBitmap(c2.pic);
		card2.setBackgroundResource(R.drawable.blub);
		c2.player = 1;
		c3.pic = getResizedBitmap(c3.pic, finalHeight, finalWidth);
		card3.setImageBitmap(c3.pic);
		card3.setBackgroundResource(R.drawable.blub);
		c3.player = 1;
		c4.pic = getResizedBitmap(c4.pic, finalHeight, finalWidth);
		card4.setImageBitmap(c4.pic);
		card4.setBackgroundResource(R.drawable.blub);
		c4.player = 1;
		c5.pic = getResizedBitmap(c5.pic, finalHeight, finalWidth);
		card5.setImageBitmap(c5.pic);
		card5.setBackgroundResource(R.drawable.blub);
		c5.player = 1;

		// keep opponents cards hidden
		c6.pic = getResizedBitmap(c6.pic, finalHeight, finalWidth);
		card6.setImageBitmap(c6.pic);
		card6.setBackgroundResource(R.drawable.redb);
		c6.player = 2;
		c7.pic = getResizedBitmap(c7.pic, finalHeight, finalWidth);
		card7.setImageBitmap(c7.pic);
		card7.setBackgroundResource(R.drawable.redb);
		c7.player = 2;
		c8.pic = getResizedBitmap(c8.pic, finalHeight, finalWidth);
		card8.setImageBitmap(c8.pic);
		card8.setBackgroundResource(R.drawable.redb);
		c8.player = 2;
		c9.pic = getResizedBitmap(c9.pic, finalHeight, finalWidth);
		card9.setImageBitmap(c9.pic);
		card9.setBackgroundResource(R.drawable.redb);
		c9.player = 2;
		c10.pic = getResizedBitmap(c10.pic, finalHeight, finalWidth);
		card10.setImageBitmap(c10.pic);
		card10.setBackgroundResource(R.drawable.redb);
		c10.player = 2;

		pcCards.clear();
		pcCards.add(c6);
		pcCards.add(c7);
		pcCards.add(c8);
		pcCards.add(c9);
		pcCards.add(c10);
	}

	private void setInitialSlots() {
		// initialize slots and blanks card in center
		slotc = (ImageButton) findViewById(R.id.slotCenter);
		slotc.setImageBitmap(cardbw);
		slotc.setPadding(pdng, pdng, pdng, pdng);

		slotr = (ImageButton) findViewById(R.id.slotRight);
		slotr.setImageBitmap(cardbw);
		slotr.setPadding(pdng, pdng, pdng, pdng);

		slotl = (ImageButton) findViewById(R.id.slotLeft);
		slotl.setImageBitmap(cardbw);
		slotl.setPadding(pdng, pdng, pdng, pdng);

		slottc = (ImageButton) findViewById(R.id.slotTopCenter);
		slottc.setImageBitmap(cardbw);
		slottc.setPadding(pdng, pdng, pdng, pdng);

		slottr = (ImageButton) findViewById(R.id.slotTopRight);
		slottr.setImageBitmap(cardbw);
		slottr.setPadding(pdng, pdng, pdng, pdng);

		slottl = (ImageButton) findViewById(R.id.slotTopLeft);
		slottl.setImageBitmap(cardbw);
		slottl.setPadding(pdng, pdng, pdng, pdng);

		slotbc = (ImageButton) findViewById(R.id.slotBotCenter);
		slotbc.setImageBitmap(cardbw);
		slotbc.setPadding(pdng, pdng, pdng, pdng);

		slotbr = (ImageButton) findViewById(R.id.slotBotRight);
		slotbr.setImageBitmap(cardbw);
		slotbr.setPadding(pdng, pdng, pdng, pdng);

		slotbl = (ImageButton) findViewById(R.id.slotBotLeft);
		slotbl.setImageBitmap(cardbw);
		slotbl.setPadding(pdng, pdng, pdng, pdng);

	}

	private void resizeCardImageButtons() {
		// scale cards to fit 5 on the screen at a time

		cardbw = BitmapFactory
				.decodeResource(getResources(), R.drawable.cardbw);
		empty = BitmapFactory.decodeResource(getResources(), R.drawable.empty);

		int cardbwW = cardbw.getWidth();
		int cardbwH = cardbw.getHeight();

		float fractionHeight = screenHeight / fracVariable;
		float divisor = cardbwH / fractionHeight;
		float newWidth = cardbwW / divisor;
		finalHeight = (int) fractionHeight;
		finalWidth = (int) newWidth;

		int x1 = metrics.widthPixels / 8;
		int x2 = 0 - metrics.widthPixels / 8;
		int y1 = 0;

		cardbw = getResizedBitmap(cardbw, finalHeight, finalWidth);

		card1 = (ImageButton) findViewById(R.id.card1);
		card1.setImageBitmap(cardbw);
		card1.setX(x1);
		card1.setY(y1);
		card2 = (ImageButton) findViewById(R.id.card2);
		card2.setImageBitmap(cardbw);
		card2.setX(x1);
		card2.setY(y1 + card1.getHeight());
		card3 = (ImageButton) findViewById(R.id.card3);
		card3.setImageBitmap(cardbw);
		card3.setX(x1);
		card3.setY(y1 + card1.getHeight() * 2);
		card4 = (ImageButton) findViewById(R.id.card4);
		card4.setImageBitmap(cardbw);
		card4.setX(x1);
		card4.setY(y1 + card1.getHeight() * 3);
		card5 = (ImageButton) findViewById(R.id.card5);
		card5.setImageBitmap(cardbw);
		card5.setX(x1);
		card5.setY(y1 + card1.getHeight() * 4);

		card6 = (ImageButton) findViewById(R.id.card6);
		card6.setImageBitmap(cardbw);
		card6.setX(x2);
		card6.setY(y1);
		card7 = (ImageButton) findViewById(R.id.card7);
		card7.setImageBitmap(cardbw);
		card7.setX(x2);
		card7.setY(y1 + card1.getHeight());
		card8 = (ImageButton) findViewById(R.id.card8);
		card8.setImageBitmap(cardbw);
		card8.setX(x2);
		card8.setY(y1 + card1.getHeight() * 2);
		card9 = (ImageButton) findViewById(R.id.card9);
		card9.setImageBitmap(cardbw);
		card9.setX(x2);
		card9.setY(y1 + card1.getHeight() * 3);
		card10 = (ImageButton) findViewById(R.id.card10);
		card10.setImageBitmap(cardbw);
		card10.setX(x2);
		card10.setY(y1 + card1.getHeight() * 4);
	}

	private void createBackground() {
		// scale image to background
		background = (ImageView) findViewById(R.id.imageViewBackground);
		bg = BitmapFactory.decodeResource(getResources(), R.drawable.board);
		bg = getResizedBitmap(bg, metrics.heightPixels, metrics.widthPixels);
		background.setImageBitmap(bg);
	}

	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
		// resized bitmap to the specs
		int w = bm.getWidth();
		int h = bm.getHeight();
		float sW = ((float) newWidth) / w;
		float sH = ((float) newHeight) / h;
		Matrix matrix = new Matrix();
		matrix.postScale(sW, sH);
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, w, h, matrix,
				false);
		return resizedBitmap;
	}

	public void card1pressed(View view) {
		// show highlight around card
		if (playerTurn == 1) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.blub);
			}

			cardSelected = true;
			currCard = c1;
			temp = card1;
			card1.setBackgroundResource(R.drawable.bluew);
			playSound(s1, fSpeed);
		}
	}

	public void card2pressed(View view) {
		if (playerTurn == 1) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.blub);
			}
			temp = card2;
			cardSelected = true;
			currCard = c2;
			card2.setBackgroundResource(R.drawable.bluew);
			playSound(s1, fSpeed);
		}
	}

	public void card3pressed(View view) {
		if (playerTurn == 1) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.blub);
			}

			cardSelected = true;
			currCard = c3;
			card3.setBackgroundResource(R.drawable.bluew);
			temp = card3;
			playSound(s1, fSpeed);
		}
	}

	public void card4pressed(View view) {
		if (playerTurn == 1) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.blub);
			}

			cardSelected = true;
			currCard = c4;
			card4.setBackgroundResource(R.drawable.bluew);
			temp = card4;
			playSound(s1, fSpeed);
		}
	}

	public void card5pressed(View view) {
		if (playerTurn == 1) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.blub);
			}

			cardSelected = true;
			currCard = c5;
			card5.setBackgroundResource(R.drawable.bluew);
			temp = card5;
			playSound(s1, fSpeed);
		}
	}

	public void card6pressed(View view) {
		if (playerTurn == 2) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.redb);
			}

			cardSelected = true;
			currCard = c6;
			card6.setBackgroundResource(R.drawable.redw);
			temp = card6;
			playSound(s1, fSpeed);
		}
	}

	public void card7pressed(View view) {
		if (playerTurn == 2) {

			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.redb);
			}

			cardSelected = true;
			currCard = c7;
			card7.setBackgroundResource(R.drawable.redw);
			temp = card7;
			playSound(s1, fSpeed);
		}
	}

	public void card8pressed(View view) {
		if (playerTurn == 2) {
			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.redb);
			}

			cardSelected = true;
			currCard = c8;
			card8.setBackgroundResource(R.drawable.redw);
			temp = card8;
			playSound(s1, fSpeed);
		}
	}

	public void card9pressed(View view) {
		if (playerTurn == 2) {
			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.redb);
			}

			cardSelected = true;
			currCard = c9;
			card9.setBackgroundResource(R.drawable.redw);
			temp = card9;
			playSound(s1, fSpeed);
		}
	}

	public void card10pressed(View view) {
		if (playerTurn == 2) {
			if (cardSelected) {
				// turn old temp back to dark blue
				temp.setBackgroundResource(R.drawable.redb);
			}

			cardSelected = true;
			currCard = c10;
			card10.setBackgroundResource(R.drawable.redw);
			temp = card10;
			playSound(s1, fSpeed);
		}
	}

	public void topcenterpressed(View view) {
		if (cardSelected && !stc.isSet) {
			// set top center card to curr card
			temp.setVisibility(View.INVISIBLE);
			stc = currCard;
			stc.isSet = true;
			slottc.setImageBitmap(stc.pic);
			cardSelected = false;
			updateBoardTopCenter();
			switchPlayer();
		}
	}

	public void botcenterpressed(View view) {
		if (cardSelected && !sbc.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			sbc = currCard;
			sbc.isSet = true;
			slotbc.setImageBitmap(sbc.pic);
			cardSelected = false;
			updateBoardBottomCenter();
			switchPlayer();
		}
	}

	public void toprightpressed(View view) {
		if (cardSelected && !str.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			str = currCard;
			str.isSet = true;
			slottr.setImageBitmap(str.pic);
			cardSelected = false;
			updateBoardTopRight();
			switchPlayer();
		}
	}

	public void rightpressed(View view) {
		if (cardSelected && !scr.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			scr = currCard;
			scr.isSet = true;
			slotr.setImageBitmap(scr.pic);
			cardSelected = false;
			updateBoardCenterRight();
			switchPlayer();
		}
	}

	public void botrightpressed(View view) {
		if (cardSelected && !sbr.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			sbr = currCard;
			sbr.isSet = true;
			slotbr.setImageBitmap(sbr.pic);
			cardSelected = false;
			updateBoardBottomRight();
			switchPlayer();
		}
	}

	public void leftpressed(View view) {
		if (cardSelected && !scl.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			scl = currCard;
			scl.isSet = true;
			slotl.setImageBitmap(scl.pic);
			cardSelected = false;
			updateBoardCenterLeft();
			switchPlayer();
		}
	}

	public void topleftpressed(View view) {
		if (cardSelected && !stl.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			stl = currCard;
			stl.isSet = true;
			slottl.setImageBitmap(stl.pic);
			cardSelected = false;
			updateBoardTopLeft();
			switchPlayer();
		}
	}

	public void botleftpressed(View view) {
		if (cardSelected && !sbl.isSet) {
			// set slot top center to curr card
			temp.setVisibility(View.INVISIBLE);
			sbl = currCard;
			sbl.isSet = true;
			slotbl.setImageBitmap(sbl.pic);
			cardSelected = false;
			updateBoardBottomLeft();
			switchPlayer();
		}
	}

	public void switchPlayer() {
		roundTurns++;
		if (roundTurns == 9) {
			if (p1Score > p2Score) {
				p1gamesWon++;
			} else if (p2Score > p1Score) {
				p2gamesWon++;
			}
			playerTurn = 0;
		} else {
			if (playerTurn == 1) {
				playerTurn = 2;
				if (cpuOpponent) {
					cpuTurn();
					switchPlayer();
				}
			} else if (playerTurn == 2) {
				playerTurn = 1;
			}
		}
	}

	public void updateBoardTopLeft() {
		switch (playerTurn) {
		case 1:
			slottl.setBackgroundResource(R.drawable.blub);
			// check tc
			if (stc != null && stc.player == 2 && stc.isSet) {
				if (stl.value > stc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					stc.player = 1;
					slottc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check cl
			if (scl != null && scl.player == 2 && scl.isSet) {
				if (stl.value > scl.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					stl.player = 1;
					slotl.setBackgroundResource(R.drawable.blub);

					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slottl.setBackgroundResource(R.drawable.redb);
			// check tr
			if (stc != null && stc.player == 1 && stc.isSet) {
				if (stl.value > stc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					stc.player = 2;
					slottc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check cl
			if (scl != null && scl.player == 1 && scl.isSet) {
				if (stl.value > scl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					scl.player = 2;
					slotl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardTopCenter() {
		switch (playerTurn) {
		case 1:
			slottc.setBackgroundResource(R.drawable.blub);
			// check left
			if (stl != null && stl.player == 2 && stl.isSet) {
				if (stc.value > stl.value) {
					p1Score++;
					p2Score--;
					// updateScoreBoard();
					stl.player = 1;
					slottl.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check right
			if (str != null && str.player == 2 && str.isSet) {
				if (stc.value > str.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					str.player = 1;
					slottr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check below
			if (sc != null && sc.player != 1 && sc.isSet) {
				if (stc.value > sc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sc.player = 1;
					slotc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slottc.setBackgroundResource(R.drawable.redb);
			// check tl
			if (stl != null && stl.player == 1 && stl.isSet) {
				if (stc.value > stl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					stl.player = 2;
					slottl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check tr
			if (str != null && str.player == 1 && str.isSet) {
				if (stc.value > str.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					str.player = 2;
					slottr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check center
			if (sc != null && sc.player != 2 && sc.isSet) {
				if (stc.value > sc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sc.player = 2;
					slotc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardTopRight() {
		switch (playerTurn) {
		case 1:
			slottr.setBackgroundResource(R.drawable.blub);
			// check left tc
			if (stc != null && stc.player == 2 && stc.isSet) {
				if (str.value > stc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					stc.player = 1;
					slottc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check below
			if (scr != null && scr.player == 2 && scr.isSet) {
				if (str.value > scr.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					scr.player = 1;
					slotr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slottr.setBackgroundResource(R.drawable.redb);
			// check tc
			if (stc != null && stc.player == 1 && stc.isSet) {
				if (str.value > stc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					stc.player = 2;
					slottc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check cl
			if (scr != null && scr.player == 1 && scr.isSet) {
				if (str.value > scr.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					scr.player = 2;
					slotr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardCenterLeft() {
		switch (playerTurn) {
		case 1:
			slotl.setBackgroundResource(R.drawable.blub);
			// check top left
			if (stl != null && stl.player == 2 && stl.isSet) {
				if (scl.value > stl.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					stl.player = 1;
					slottl.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check center
			if (sc != null && sc.player != 1 && sc.isSet) {
				if (scl.value > sc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sc.player = 1;
					slotc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check below
			if (sbl != null && sbl.player == 2 && sbl.isSet) {
				if (scl.value > sbl.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbl.player = 1;
					slotbl.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slotl.setBackgroundResource(R.drawable.redb);
			// check tl
			if (stl != null && stl.player == 1 && stl.isSet) {
				if (scl.value > stl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					stl.player = 2;
					slotl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check c
			if (sc != null && sc.player != 2 && sc.isSet) {
				if (scl.value > sc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sc.player = 2;
					slotc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check bl
			if (sbl != null && sbl.player == 1 && sbl.isSet) {
				if (scl.value > sbl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sbl.player = 2;
					slotbl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardCenterRight() {
		switch (playerTurn) {
		case 1:
			slotr.setBackgroundResource(R.drawable.blub);
			// check tr
			if (str != null && str.player == 2 && str.isSet) {
				if (scr.value > str.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					str.player = 1;
					slottr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check center
			if (sc != null && sc.player != 1 && sc.isSet) {
				if (scr.value > sc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sc.player = 1;
					slotc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check br
			if (sbr != null && sbr.player == 2 && sbr.isSet) {
				if (scr.value > sbr.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbr.player = 1;
					slotbr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slotr.setBackgroundResource(R.drawable.redb);
			// check tr
			if (str != null && str.player == 1 && str.isSet) {
				if (scr.value > str.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					stc.player = 2;
					slottr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check c
			if (sc != null && sc.player != 2 && sc.isSet) {
				if (scr.value > sc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sc.player = 2;
					slotc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check br
			if (sbr != null && sbr.player == 1 && sbr.isSet) {
				if (scr.value > sbr.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					scr.player = 2;
					slotbr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardBottomLeft() {
		switch (playerTurn) {
		case 1:
			slotbl.setBackgroundResource(R.drawable.blub);
			// check above cl
			if (scl != null && scl.player == 2 && scl.isSet) {
				if (sbl.value > scl.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					scl.player = 1;
					slotl.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check right bc
			if (sbc != null && sbc.player == 2 && sbc.isSet) {
				if (sbl.value > sbc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbc.player = 1;
					slotbc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slotbl.setBackgroundResource(R.drawable.redb);
			// check tl
			if (scl != null && scl.player == 1 && scl.isSet) {
				if (sbl.value > scl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					scl.player = 2;
					slotl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check bc
			if (sbc != null && sbc.player == 1 && sbc.isSet) {
				if (sbl.value > sbc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sbc.player = 2;
					slotbc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardBottomCenter() {
		switch (playerTurn) {
		case 1:
			slotbc.setBackgroundResource(R.drawable.blub);
			// check up sc
			if (sc != null && sc.player != 1 && sc.isSet) {
				if (sbc.value > sc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sc.player = 1;
					slotc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check left bl
			if (sbl != null && sbl.player == 2 && sbl.isSet) {
				if (sbc.value > sbl.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbl.player = 1;
					slotbl.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check br
			if (sbr != null && sbr.player == 2 && sbr.isSet) {
				if (sbc.value > sbr.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbr.player = 1;
					slotbr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slotbc.setBackgroundResource(R.drawable.redb);
			// check sc
			if (sc != null && sc.player != 2 && sc.isSet) {
				if (sbc.value > sc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sc.player = 2;
					slotc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check bl
			if (sbl != null && sbl.player == 1 && sbl.isSet) {
				if (sbc.value > sbl.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sbl.player = 2;
					slotbl.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check br
			if (sbr != null && sbr.player == 1 && sbr.isSet) {
				if (sbc.value > sbr.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sbr.player = 2;
					slotbr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardBottomRight() {
		switch (playerTurn) {
		case 1:
			slotbr.setBackgroundResource(R.drawable.blub);
			// check up cr
			if (scr != null && scr.player == 2 && scr.isSet) {
				if (sbr.value > scr.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					scr.player = 1;
					slotr.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			// check bc
			if (sbc != null && sbc.player == 2 && sbc.isSet) {
				if (sbr.value > sbc.value) {
					p1Score++;
					p2Score--;
					updateScoreBoard();
					sbc.player = 1;
					slotbc.setBackgroundResource(R.drawable.blub);
					playSound(s2, fSpeed);
				}
			}
			break;
		case 2:
			slotbr.setBackgroundResource(R.drawable.redb);
			// check cr
			if (scr != null && scr.player == 1 && scr.isSet) {
				if (sbr.value > scr.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					scr.player = 2;
					slotr.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			// check bc
			if (sbc != null && sbc.player == 1 && sbc.isSet) {
				if (sbr.value > sbc.value) {
					p1Score--;
					p2Score++;
					updateScoreBoard();
					sbc.player = 2;
					slotbc.setBackgroundResource(R.drawable.redb);
					playSound(s2, fSpeed);
				}
			}
			break;
		}
	}

	public void updateBoardCenter() {
	}

	public void updateScoreBoard() {
		getCurrentScoreString();
		scoreText.setText(scoreString);
	}

	public void getCurrentScoreString() {
		scoreString = "Player 1 Wins = " + p1gamesWon + "    [" + p1Score
				+ " - " + p2Score + "]    " + "Player 2 Wins = " + p2gamesWon;
	}

}
