package card.war;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import android.content.Context;
import android.graphics.BitmapFactory;

public class Deck {

	Card[] cards = new Card[52];
	int[] values = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 };
	String[] faces = { "heart", "diamond", "spade", "club" };
	int currCard = 0;
	String path;
	URL url;
	int cardH, cardW;
	int i;
	Random rand;
	Card[] shuffled;

	public Deck(Context c) throws IOException {
		rand = new Random();
		i = 0;
		currCard = 0;
		for (String f : faces) {
			for (int v : values) {
				cards[i] = new Card(v, f);
				switch (i) {
				case 0:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twoh);
					break;
				case 1:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.threeh);
					break;
				case 2:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fourh);
					break;
				case 3:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fiveh);
					break;
				case 4:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sixh);
					break;
				case 5:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sevenh);
					break;
				case 6:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.eighth);
					break;
				case 7:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.nineh);
					break;
				case 8:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.tenh);					
					break;
				case 9:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.elevenh);
					break;
				case 10:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twelveh);
					break;
				case 11:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.thirtteenh);
					break;
				case 12:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.aceh);
					break;
				case 13:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twod);					
					break;
				case 14:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.threed);
					break;
				case 15:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fourd);
					break;
				case 16:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fived);
					break;
				case 17:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sixd);
					break;
				case 18:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sevend);
					break;
				case 19:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.eightd);
					break;
				case 20:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.nined);
					break;
				case 21:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.tend);
					break;
				case 22:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.elevend);
					break;
				case 23:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twelved);
					break;
				case 24:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.thirtteend);
					break;
				case 25:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.aced);
					break;
				case 26:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twos);
					break;
				case 27:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.threes);
					break;
				case 28:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fours);
					break;
				case 29:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fives);
					break;
				case 30:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sixs);
					break;
				case 31:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sevens);
					break;
				case 32:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.eights);
					break;
				case 33:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.nines);
					break;
				case 34:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.tens);
					break;
				case 35:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.elevens);
					break;
				case 36:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twelves);
					break;
				case 37:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.thirtteens);
					break;
				case 38:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.aces);
					break;
				case 39:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twoc);
					break;
				case 40:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.threec);
					break;
				case 41:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fourc);
					break;
				case 42:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.fivec);
					break;
				case 43:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sixc);
					break;
				case 44:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.sevenc);
					break;
				case 45:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.eightc);
					break;
				case 46:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.ninec);
					break;
				case 47:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.tenc);
					break;
				case 48:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.elevenc);
					break;
				case 49:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.twelvec);
					break;
				case 50:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.thirtteenc);
					break;
				case 51:
					cards[i].pic = BitmapFactory.decodeResource(c.getResources(),R.drawable.acec);
					break;
				}
				i++;
			}
		}
	}

	public void shuffle() {
		currCard = 0;
		shuffled = new Card[52];

		ArrayList<Integer> randomOrder = new ArrayList<Integer>();
		int x = rand.nextInt(52);

		while (randomOrder.size() < 52) {
			if (randomOrder.contains(x)) {
				x = rand.nextInt(52);
			} else {
				randomOrder.add(x);
				x = rand.nextInt(52);
			}
		}

		for (int i = 0; i < 52; i++) {
			x = randomOrder.remove(0);
			shuffled[i] = cards[x];
		}

		cards = shuffled;
	}

	public Card getNextCard() {
		Card c = cards[currCard];
		c.isSet = false;
		currCard++;
		return c;
	}

}
